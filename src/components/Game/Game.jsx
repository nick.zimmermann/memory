import React from "react";
import Card from "../Card/Card";
import useCards from "../../hooks/useCards";
import "./Game.css";

const Game = ({ randomColor }) => {
  const { cards, flipCard, tries, resetGame, seconds } = useCards();

  const renderCards = () => {
    if (cards) {
      return (
        <div className="game-container">
          {cards.map((card, index) => (
            <Card
              randomColor={randomColor}
              card={card}
              clicked={() => flipCard(card)}
              key={index}
            />
          ))}
        </div>
      );
    }
  };

  return (
    <div className="game">
      <div className="score">
        <p>Score: {tries}</p>
        <p>Timer: {seconds}</p>
      </div>
      <button
        className="reset"
        onClick={resetGame}
        style={{ backgroundColor: `#${randomColor}75` }}
      >
        Reset
      </button>
      <div className="title">
        <h1>Memory</h1>
      </div>
      {renderCards()}
    </div>
  );
};

export default Game;
