import React from "react";
import "./Card.css";

const Card = ({ card, clicked, randomColor }) => {
  const showImage = card.isOpen
    ? { backgroundImage: `url(${card.url})` }
    : null;
  const classes = card.isOpen ? "card card-open" : "card";

  return (
    <div className="card-container">
      <div className={classes} onClick={clicked}>
        <div
          className="front"
          style={{ backgroundColor: `#${randomColor}` }}
        ></div>
        <div className="back" style={showImage}></div>
      </div>
    </div>
  );
};

export default Card;
