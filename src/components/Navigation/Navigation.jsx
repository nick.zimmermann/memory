import React, { useState } from "react";
import { Link } from "react-router-dom";
import "./Navigation.css";

const Navigation = ({ randomColor }) => {
  const [navOpen, setNavOpen] = useState("nav-container");

  const navToggle = () => {
    navOpen === "nav-container"
      ? setNavOpen("nav-container nav-open")
      : setNavOpen("nav-container");
  };

  const closeNav = () => setNavOpen("nav-container");

  return (
    <nav className={navOpen} style={{ backgroundColor: `#${randomColor}` }}>
      <ul>
        <li>
          <Link
            to="/"
            onClick={closeNav}
            style={{ backgroundColor: `#${randomColor}` }}
          >
            Memory
          </Link>
        </li>
        <li>
          <Link
            to="/scoreboard"
            onClick={closeNav}
            style={{ backgroundColor: `#${randomColor}` }}
          >
            Scoreboard
          </Link>
        </li>
      </ul>
      <div id="menu-toggle" onClick={navToggle}>
        <span className="hamburger"></span>
      </div>
    </nav>
  );
};
export default Navigation;
