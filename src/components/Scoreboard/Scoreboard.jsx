import React from "react";
import "./Scoreboard.css";
import useScores from "../../hooks/useScores";

const ScoreBoard = () => {
  const { scores } = useScores();

  return (
    <div className="scoreboard">
      <div className="scoreboard-container">
        <h1>Scoreboard</h1>
        <table>
          <thead>
            <tr>
              <th>Rang</th>
              <th>Name</th>
              <th>Versuche</th>
            </tr>
          </thead>
          <tbody>
            {scores.map(score => {
              return (
                <tr key={score._id}>
                  <td>{score.rank}</td>
                  <td>
                    <a href={`/scoreboard/${score.username}`}>
                      {score.username}
                    </a>
                  </td>
                  <td>{score.score}</td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    </div>
  );
};
export default ScoreBoard;
