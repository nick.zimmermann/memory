import React from "react";
import useScores from "../../hooks/useScores";
import "./Scoreboard-user.css";
import { useParams } from "react-router-dom";

const ScoreUser = ({ randomColor }) => {
  let { username } = useParams();

  const { scores } = useScores();

  const filteredScores = scores.filter(score => score.username === username);

  const renderScores = () => {
    if (filteredScores.length > 0) {
      return (
        <div className="scoreboard-user-container">
          <h1>Scores from {username}</h1>
          <table>
            <thead>
              <tr>
                <th>Rang</th>
                <th>Name</th>
                <th>Versuche</th>
              </tr>
            </thead>
            <tbody>
              {filteredScores.map((score, index) => {
                return (
                  <tr key={index}>
                    <td>{score.rank}</td>
                    <td>{score.username}</td>
                    <td>{score.score}</td>
                  </tr>
                );
              })}
            </tbody>
          </table>
        </div>
      );
    }
    if (filteredScores.length === 0) {
      return (
        <div className="scoreboard-user-container">
          <h1>Es existieren keine Daten für diesen Username</h1>
        </div>
      );
    }
  };

  return (
    <div className="scoreboard-user">
      <a
        className="goBack"
        href="../scoreboard"
        style={{ backgroundColor: `#${randomColor}` }}
      >
        &#x279c;
      </a>
      {renderScores()}
    </div>
  );
};

export default ScoreUser;
