import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import Nav from "./components/Navigation/Navigation";
import Scoreboard from "./components/Scoreboard/Scoreboard";
import Game from "./components/Game/Game";
import ScoreUser from "./components/Scoreboard-user/Scoreboard-user";
import "./styles/_index.css";
import { shuffle, first } from "lodash/fp";

const App = () => {
  let colors = [
    "2971c4", // Dunkles Blau
    "00bFFF", // Helles Blau
    "20b2aa", // Dunkles Grün
    "9acd32", // Helles Grün
    "ff6F61", // Helles Rot
    "ff7700" // Orange
  ];
  colors = shuffle(colors);
  const randomColor = first(colors);

  return (
    <Router>
      <Nav randomColor={randomColor} />
      <Switch>
        <Route exact path="/">
          <Game randomColor={randomColor} />
        </Route>
        <Route exact path="/scoreboard">
          <Scoreboard randomColor={randomColor} />
        </Route>
        <Route path="/scoreboard/:username">
          <ScoreUser randomColor={randomColor} />
        </Route>
      </Switch>
    </Router>
  );
};

export default App;
