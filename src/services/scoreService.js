import axios from 'axios';
import config from '../config/config';

export const getAllScores = async () => {
  const scores = await axios.get(`${config.backendUrl}/scores`);
  return scores.data;
};

export const scoreFromSpecificUser = async username => {
  const scoresOfUser = await axios.get(`${config.backendUrl}/scores/${username}`);
  return scoresOfUser.data;
};

export const createScore = async (username, score) => {
  const result = await axios
    .post(`${config.backendUrl}/scores`, {
      username: username,
      score: score
    });
  return result;
};
