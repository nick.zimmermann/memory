import config from '../config/config';

export const getAllCards = async () => {
  return fetch(`${config.remoteUrl}/cards`).then(res => {
    return res ? res.json() : false;
  });
};
