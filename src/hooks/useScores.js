import { useState, useEffect } from "react";
import { getAllScores } from "../services/scoreService";

const useScores = () => {
  const [scores, setScores] = useState([]);

  useEffect(() => {
    const fetchScores = async () => {
      // Gets all cards
      const allScores = await getAllScores();

      let filteredScore = allScores.filter(score => score.score !== undefined && score.score >= 8 && score.score <= 100
      );

      filteredScore = filteredScore.slice(filteredScore.length - 20, filteredScore.length);

      const sortedScore = filteredScore.sort((a, b) => a.score - b.score);

      // Used to calculate a real ranking(same count of tries are on the same rank)
      const groupedScores = sortedScore.reduce((accumulator, scoreItem) => {
        if (accumulator[scoreItem.score]) {
          accumulator[scoreItem.score].push(scoreItem);
          return accumulator;
        }
        return { ...accumulator, [scoreItem.score]: [scoreItem] };
      }, {});

      // Add a rank property to all scoreItems based on the count of scoreItems before
      setScores(
        Object.values(groupedScores)
          .map((scoreGroup, index, mapArray) =>
            scoreGroup.map(item => ({
              ...item,
              rank:
                mapArray
                  .filter((element, filterIndex) => filterIndex < index)
                  .flat().length + 1
            }))
          )
          .flat()
      );
    };

    fetchScores();
  }, []);

  return { scores };
};

export default useScores;
