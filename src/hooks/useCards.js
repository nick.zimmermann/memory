import { shuffle } from 'lodash/fp';
import { useState, useEffect, useRef } from 'react';
import { getAllCards } from '../services/cardService';
import { createScore } from '../services/scoreService';

const useCards = () => {
  const [tries, setTries] = useState(0);
  const [cards, setCards] = useState([]);
  const [openCards, setOpenCards] = useState([]);
  const [finishedCards, setFinishedCards] = useState([]);
  const [seconds, setSeconds] = useState(60);
  const [isRunning, setIsRunning] = useState(false);
  const timerId = useRef();

  useEffect(() => {
    const fetchCards = async () => {
      // Gets all cards
      const allCards = await getAllCards();

      const shuffeledCards = shuffle(allCards);
      const randomCards = shuffeledCards.slice(0, 8);

      // Duplicate cards
      const duplicatedCards = [...randomCards];
      let cards = randomCards.concat(duplicatedCards);
      // Switch order of cards
      cards = shuffle(cards);

      // add isOpen and isFound to cards
      const mappedCards = cards.map((card, index) => {
        return {
          ...card,
          id: index,
          isOpen: false,
          isFound: false
        };
      });

      setCards(mappedCards);
    };

    fetchCards();
  }, []);

  useEffect(() => {
    if (openCards.length !== 2) return;

    setTries(tries => tries + 1);

    const [firstCard, secondCard] = openCards;

    // Check if first card and second card match
    if (firstCard.url === secondCard.url && firstCard.id !== secondCard.id) {
      // Set isFound to true where id is same
      setCards(cards =>
        cards.map(card => {
          if (card.id === firstCard.id || card.id === secondCard.id) return { ...card, isFound: true };
          return card;
        })
      );
      setOpenCards([]);
      // Increase count of increased card
      // setFinishedCards(tries => tries + 2);
      setFinishedCards(finishedCards.concat(firstCard, secondCard));
      
    } else {
      // Set timeout of 1s and set isOpen to false
      setTimeout(() => {
        setCards(cards =>
          cards.map(card => {
            if (card.id === firstCard.id || card.id === secondCard.id) return { ...card, isOpen: false };
            return card;
          })
        );
        setOpenCards([]);
      }, 1000);
    }

    // completed();
    // eslint-disable-next-line
  }, [openCards]);

  const flipCard = clickedCard => {
    if (clickedCard.isFound || clickedCard.isOpen) return;
    if (!isRunning) setIsRunning(true);
    // Add card to openCards array
    setOpenCards(openCards.concat(clickedCard));
    
    if (openCards.length >= 2) return;

    setCards(cards =>
      cards.map((card, index) => {
        if (clickedCard.id === index) return { ...card, isOpen: true };
        return card;
      })
    );
  };

  useEffect(() => {
    const completed = async () => {
      if (finishedCards.length !== 16) return;
      clearTimer();
      const name = prompt(`Dein Name, Punktzahl: ${tries}`);
      if (name !== null && name !== undefined && name !== '') {
        await createScore(name, tries);
        window.location.href = '/scoreboard';
      }
    };

    completed();
  }, [finishedCards, tries]);

  const resetGame = () => {
    document.location.reload();
  };

  // Timer

  const clearTimer = () => {
    clearInterval(timerId.current);
  };

  useEffect(() => {
    if(isRunning){
      timerId.current = setInterval(() => {
        setSeconds(seconds => seconds - 1);
      }, 1000);
    }
    return () => clearTimer();
    
    // eslint-disable-next-line
  }, [isRunning]);

  useEffect(() => {
    if(seconds <= 0){
      clearTimer();
      alert('Times up');
      resetGame();
    }
  }, [seconds]);

  return { cards, flipCard, tries, resetGame, seconds };
};

export default useCards;
