/* eslint-disable */

module.exports = {
  parser: 'postcss',
  plugins: [
    require('postcss-custom-properties')({
      importFrom: ['./src/styles/variables/global.css', './src/styles/variables/colors.css'],
    }),
    // require('postcss-cssnext')({})
  ],
};
